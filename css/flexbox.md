# Flexbox

#### Vocabulaire

> La propriété `flex` est une propriété raccourcie qui définit la capacité d'un élément flexible à modifier ses dimensions 
afin de remplir l'espace disponible. Les éléments flexibles peuvent être étirés ou réduits pour utiliser un espace 
proportionnel à leur coefficient de grossissement ou de rétrécissement afin de ne pas dépasser d'un conteneur.

![alt text](images/flexbox/vocab.png "flexbox")

#### Layout

![alt text](images/flexbox/layout.png "flexbox layout")

```css
.container {
    display: flex;
}
```

#### Flex-direction

> La propriété `flex-direction` indique la façon dont les éléments flexibles sont placés dans un conteneur flexible : 
elle définit l'axe principal et la direction des éléments (normale ou inversée).

![alt text](images/flexbox/layout.png "flexbox layout")

```css
.container {
    display: flex;
    flex-direction: row;
}
```

- `row` : L'axe principal du conteneur flexible suit la direction du texte. Les points main-start et main-end vont dans 
la même direction que le contenu.

![alt text](images/flexbox/flex-direction-row.png "flex-direction: row")

```css
.container {
    display: flex;
    flex-direction: row;
}
```

- `row-reverse` : L'axe principal du conteneur flexible suit la direction du texte. Les points main-start et main-end 
vont dans la direction opposée au contenu.

![alt text](images/flexbox/flex-direction-row-reverse.png "flex-direction: row-reverse")

```css
.container {
    display: flex;
    flex-direction: row-reverse;
}
```

- `column` : L'axe principal du conteneur flexible suit l'axe de bloc (perpendiculaire à la direction du texte). 
Les points main-start et main-end correspondent aux points before et after de writing-mode.

![alt text](images/flexbox/flex-direction-column.png "flex-direction: column")

```css
.container {
    display: flex;
    flex-direction: column;
}
```

- `column-reverse` : Se comporte comme column mais main-start et main-end sont échangés.

![alt text](images/flexbox/flex-direction-column-reverse.png "flex-direction: column-reverse")

```css
.container {
    display: flex;
    flex-direction: column-reverse;
}
```

#### Axis

![alt text](images/flexbox/axis.png "axis")

#### Flex-wrap

> La propriété `flex-wrap` indique si les éléments flexibles sont contraints à être disposés sur une seule ligne ou s'ils 
peuvent être affichés sur plusieurs lignes avec un retour automatique. Si le retour à la ligne est autorisé, la 
propriété permet également de contrôler la direction dans laquelle les lignes sont empilées.

- `nowrap` : Les éléments flexibles sont disposés sur une seule ligne. Cela peut entraîner un dépassement du conteneur. 
La ligne cross-start est équivalente à start ou à before selon la valeur de flex-direction.

![alt text](images/flexbox/flex-wrap-nowrap.png "flex-wrap: nowrap")

```css
.container {
    display: flex;
    flex-direction: row; 
    flex-wrap: nowrap;
}
```

- `wrap` : Les éléments flexibles sont disposé sur plusieurs lignes. La ligne cross-start est équivalente à  start ou 
before en fonction de la valeur de flex-direction et la ligne cross-end est à l'opposée cross-start.

![alt text](images/flexbox/flex-wrap-wrap.png "flex-wrap: wrap")

```css
.container {
    display: flex;
    flex-direction: row; 
    flex-wrap: wrap;
}
```

- `wrap-reverse` : Se comporte comme wrap mais cross-start et cross-end sont permutées.

```css
.container {
    display: flex;
    flex-direction: row; 
    flex-wrap: wrap-reverse;
}
```

#### flex-grow

> La propriété `flex-grow` indique le facteur de grossissement d'un élément flexible. Elle indique la quantité d'espace 
que l'élément devrait consommer dans un conteneur flexible.

- `<number>` : Un nombre qui correspond au facteur de grossissement utilisé. Plus la valeur est élevée, plus l'élément 
sera étendu si nécessaire. Les valeurs négatives sont invalides.

```html
<div id="content">
  <div class="box" style="background-color:red;">A</div>
  <div class="box" style="background-color:lightblue;">B</div>
  <div class="box" style="background-color:yellow;">C</div>
  <div class="box1" style="background-color:brown;">D</div>
  <div class="box1" style="background-color:lightgreen;">E</div>
  <div class="box" style="background-color:brown;">F</div>
</div>
```

```css
#content {
  -ms-box-orient: horizontal;
  display: -webkit-box;
  display: -moz-box;
  display: -ms-flexbox;
  display: -moz-flex;
  display: -webkit-flex;
  display: flex;
    
  -webkit-justify-content: space-around;
  justify-content: space-around;
  -webkit-flex-flow: row wrap;
  flex-flow: row wrap;
  -webkit-align-items: stretch;
  align-items: stretch;
}

.box { 
  flex-shrink: 1; 
  border: 3px solid rgba(0,0,0,.2);
}

.box1 { 
  flex-grow: 1; 
  border: 3px solid rgba(0,0,0,.2);
}
```

![alt text](images/flexbox/flex-grow.png "flex-grow")

#### flex-shrink

> La propriété `flex-shrink` indique le facteur de rétrécissement d'un élément flexible.

- `<number>` : Un nombre (type <number> qui correspond au facteur de rétrécissement utilisé. Plus la valeur est élevée, 
plus l'élément sera compressé si nécessaire. Les valeurs négatives sont invalides.


```html
<div id="content">
  <div class="box" style="background-color:red;">A</div>
  <div class="box" style="background-color:lightblue;">B</div>
  <div class="box" style="background-color:yellow;">C</div>
  <div class="box1" style="background-color:brown;">D</div>
  <div class="box1" style="background-color:lightgreen;">E</div>
  <div class="box" style="background-color:brown;">F</div>
</div>
```

```css
#content {
  display: flex;
  width: 500px;
}

#content div {
  flex-basis: 120px;
  border: 3px solid red;
}

.box { 
  flex-shrink: 1; 
}

.box1 { 
  flex-shrink: 2; 
}
```

![alt text](images/flexbox/flex-shrink.png "flex-shrink")

#### order

> La propriété `order` définit l'ordre avec lequel on dessine les éléments flexibles dans leur conteneur flexible. Les 
éléments sont appliqués dans l'ordre croissant des valeurs de order. Les éléments pour lesquels la valeur de order est 
la même seront appliqués dans l'ordre avec lequel ils apparaissent dans le code source du document.

- `<integer>` : Un nombre entier indiquant l'ordre à appliquer pour l'élément flexible.

```html
<div id="main">
   <article>Lorem ipsum</article>
   <nav>consectetur adipisicing elit, sed do eiusmod</nav>
   <aside>tempor incididunt ut labore et dolore magna</aside>
</div>
```

```css
#main {
  display: flex;
}

#main > article {
  flex:1;
  order: 2;
  border: 1px dotted orange;
}

#main > nav {
  width: 200px;
  order: 1;
  border: 1px dotted blue;
}

#main > aside {
  width: 200px;
  order: 3;
  border: 1px dotted blue;
}
```

![alt text](images/flexbox/order.png "order")

#### justify-content

> La propriété CSS `justify-content` indique la façon dont l'espace doit être réparti entre et autour des éléments selon 
l'axe principal du conteneur.

- `start` : Les éléments sont regroupés au début du conteneur selon l'axe principal. Le bord du premier élément est 
aligné avec le bord du conteneur.

```css
.container {
	display: flex;
	justify-content: start; 
}
```

- `end` : Les éléments sont regroupés à la fin du conteneur selon l'axe principal. Le bord du dernier élément est 
aligné avec le bord du conteneur.

```css
.container {
	display: flex;
	justify-content: end; 
}
```

- `flex-start` : Les éléments sont regroupés vers le début du conteneur, selon l'axe principal et le sens du conteneur flexible.
Cette valeur ne s'applique qu'aux éléments flexibles. Pour les éléments qui ne sont pas dans un conteneur flexible, cette 
valeur est synonyme de start.

```css
.container {
	display: flex;
	justify-content: flex-start; 
}
```

- `flex-end` : Les éléments sont regroupés vers la fin du conteneur, selon l'axe principal et le sens du conteneur flexible.
Cette valeur ne s'applique qu'aux éléments flexibles. Pour les éléments qui ne sont pas dans un conteneur flexible, cette 
valeur est synonyme de end.

```css
.container {
	display: flex;
	justify-content: flex-end; 
}
```

- `center` : Les éléments sont regroupés au centre du conteneur selon l'axe principal.

```css
.container {
	display: flex;
	justify-content: center; 
}
```

- `left` : Les éléments sont regroupés vers le bord gauche du conteneur. Si l'axe n'est pas parallèle à l'axe en ligne, 
cette valeur est synonyme de start.

```css
.container {
	display: flex;
	justify-content: left; 
}
```

- `right` : Les éléments sont regroupés vers le bord droit du conteneur. Si l'axe n'est pas parallèle à l'axe en ligne, 
cette valeur est synonyme de start.


```css
.container {
	display: flex;
	justify-content: right; 
}
```

- `baseline`, `first baseline`, `last baseline` : Ces valeurs permettent d'aligner les éléments selon les différentes 
lignes de base. En cas de besoin first baseline correspondra à start, et last baseline correspondra à end.

```css
.container {
	display: flex;
	justify-content: baseline; 
}
```

- `space-between` : Les éléments ont espacés équitablement selon l'axe principal. L'espace utilisé entre chaque élément 
est le même. Le premier élément est aligné sur le bord du conteneur et le dernier élément est aussi aligné sur le bord de l'élément.

```css
.container {
	display: flex;
	justify-content: space-around; 
}
```

- `space-around` : Les éléments ont espacés équitablement selon l'axe principal. L'espace utilisé entre chaque élément 
est le même. L'espace entre le bord du conteneur et le premier élément et l'espace entre le dernier élément et le bord 
du conteneur représente la moitié de l'espace entre deux éléments.

```css
.container {
	display: flex;
	justify-content: space-around; 
}
```

- `space-evenly` : Les éléments sont espacés équitablement. L'espace utilisé entre chaque élément, entre le bord du 
conteneur et le premier élément, et entre le dernier élément et le bord du conteneur est le même.

```css
.container {
	display: flex;
	justify-content: space-evenly; 
}
```

- `stretch` : Si la somme des tailles des éléments est inférieure à la taille du conteneur, tous les éléments 
dimensionnés avec auto sont agrandis avec le même supplément, tout en respectant les contraintes imposées par 
max-height/max-width (ou par les fonctionnalités analogues). Ainsi, l'ensemble des éléments remplit exactement le 
conteneur sur l'axe principal.

```css
.container {
	display: flex;
	justify-content: stretch; 
}
```

- `safe` : Si l'élément dépasse du conteneur, il est aligné comme avec la valeur start.

```css
.container {
	display: flex;
	justify-content: safe; 
}
```

- `unsafe` : Quelle que soit les tailes relatives des éléments par rapport au conteneur, l'alignement indiqué est respecté.

```css
.container {
	display: flex;
	justify-content: unsafe; 
}
```

#### align-items

> La propriété CSS `align-items` définit la façon dont l'espace est réparti autour des éléments flexibles le long de 
l'axe en bloc du conteneur. Autrement dit, cette propriété fonctionne comme justify-items mais dans la direction perpendiculaire.


- `normal` : L'effet obtenu avec ce mot-clé dépend du mode de disposition utilisé :
  - Pour les éléments positionnés de façon absolue, ce mot-clé est synonyme de start pour les éléments remplacés, pour 
  les autres éléments positionnés de façon absolue, il est synonyme de stretch.
  - Pour les éléments avec une position statique sur une disposition absolue, ce mot-clé se comporte comme stretch.
  - Pour les éléments flexibles, ce mot-clé est synonyme de stretch.
  - Pour les éléments d'une grille, ce mot-clé se comportera comme stretch sauf pour les boîtes ayant des dimensions 
  intrinsèques où il se comporte comme start.
  
  Cette propriété ne s'applique pas aux boîtes en bloc ou aux cellules de tableaux.



- `flex-start` : Le bord de la marge de l'élément flexible sur l'axe en bloc est aligné avec le bord de la ligne au 
début de l'axe en bloc.



- `flex-end` : Le bord de la marge de l'élément flexible sur l'axe en bloc est aligné avec le bord de la ligne à la fin 
de l'axe en bloc.



- `center` : L'élément flexible est centrée sur l'axe en bloc au sein de la ligne. Si cet élément est plus grand que la 
ligne, il dépassera également de chaque côté.



- `start` : L'élément est aligné sur le bord au début du conteneur selon l'axe de bloc.

- `end` : L'élément est aligné sur le bord à la fin du conteneur selon l'axe de bloc

- `center` : L'élément est centré sur l'axe en bloc du conteneur.



- `left` : Les éléments sont regroupés sur la gauche du conteneur. Si l'axe de la propriété n'est pas parallèle à l'axe 
en ligne, cette valeur est synonyme de start.



- `right` : Les éléments sont regroupés sur la droite du conteneur. Si l'axe de la propriété n'est pas parallèle à l'axe 
en ligne, cette valeur est synonyme de start.



- `self-start` : L'élément est accolé au bord du début du conteneur dans l'axe de bloc.

- `self-end` : L'élément est accolé au bord à la fin du conteneur dans l'axe de bloc.


- `baseline`, `first baseline`, `last baseline` : Tous les éléments flexibles sont alignés afin que leurs différentes 
lignes de base soient alignées. L'élément pour lequel on a la plus grande distance entre la marge et la ligne de base 
est aligné sur le bord de la ligne courante.



- `stretch` : Les éléments flexibles sont étirés afin que la taille de la boîte de marge sur l'axe en bloc est la même 
que celle de la ligne sur laquelle l'élément se trouve, tout en respectant les contraintes de hauteur et de largeur.



-  `safe` : Si la taille d'un des éléments dépasse du conteneur avec la valeur d'alignement indiquée, l'alignement sera 
réalisé avec la valeur start à la place.

- `unsafe` : Quelle que soit la taille relative et l'éventuel dépassement de l'élément par rapport au conteneur, la 
valeur indiquée pour l'alignement est respectée.

#### align-content

> La propriété CSS `align-content définit la façon dont l'espace est réparti entre et autour des éléments le long de
l'axe en bloc du conteneur (c'est-à-dire l'axe orthogonal à l'axe d'écriture) lorsque celui-ci est un conteneur de boîte flexible.


- `start` : Les éléments sont regroupés vers le bord au début de l'axe de bloc.`



- `end`  : Les éléments sont regroupés vers le bord à la fin de l'axe de bloc.



- `flex-start` : Les éléments flexibles sont regroupés vers le bord au début de l'axe de bloc. Cette valeur ne s'applique 
qu'aux éléments fils d'un conteneur flexible, sinon elle est synonyme de start.



- `flex-end` : Les éléments flexibles sont regroupés vers le bord au début de l'axe de bloc. Cette valeur ne s'applique 
qu'aux éléments fils d'un conteneur flexible, sinon elle est synonyme de end.



- `center` : Les éléments sont regroupés au centre de l'axe de bloc.



- `left` : Les éléments sont regroupés vers le bord gauche du conteneur. Si l'axe n'est pas parallèle à l'axe en ligne, 
cette valeur se comporte comme start.



- `right` : Les éléments sont regroupés vers le bord droit du conteneur. Si l'axe n'est pas parallèle à l'axe en ligne, 
cette valeur se comporte comme start.



- `baseline`, `first baseline`, `last baseline` : Ces valeurs permettent de définir l'alignement par rapport à la ligne 
de base pour l'élément du conteneur avec la ligne de base la plus haute ou la plus basse. Si first baseline n'est pas 
prise en charge, la valeur correspondra à start, si last baseline n'est pas prise en charge, la valeur correspondra à end.



- `space-between` : Les éléments sont équirépartis le long de l'axe en bloc. L'espace obtenu est le même entre chaque 
élément, le premier élément est aligné sur le bord au début du conteneur et le dernier élément est aligné sur le bord à 
la fin du conteneur.`



- `space-around` : Les éléments sont équirépartis le long de l'axe en bloc. L'espace obtenu est le même entre chaque 
élément, la moitié de cet espace est utilisée entre le premier élément et le bord au début du conteneur et la moitié de 
cet espace est utilisée entre le dernier élément et le bord à la fin du conteneur`



- `space-evenly` : Les éléments sont équirépartis le long de l'axe en bloc. L'espace obtenu est le même entre chaque 
élément, entre le premier élément et le bord du conteneur et entre le dernier élément et le bord du conteneur.`



- `stretch` : Si la somme des tailles des éléments est inférieure à la taille du conteneur pour l'axe en bloc, les 
éléments dimensionnés automatiquement seront élargis de la même longueur tout en respectant les contraintes imposées par 
max-height/max-width (ou par les fonctionnalités équivalentes), afin que l'ensemble des éléments remplisse exactement le conteneur.`



- `safe` : Si l'élément dépasse du conteneur avec la valeur d'alignement indiquée, l'élément sera alors aligné avec la valeur start.`



- `unsafe` : Quelle que soit la taille et le dépassement éventuellement occasionné, l'élément est aligné avec la valeur indiquée.`



