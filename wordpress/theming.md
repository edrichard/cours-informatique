# Theme enfant de WordPress

> Un thème enfant WordPress est un thème qui hérite des fonctionnalités d'un autre thème, appelé thème parent. Le thème enfant est la méthode recommandée pour modifier un thème existant.

Les thèmes WordPress se trouve dans le répertoire  `wp-content/themes/`.

#### Pourquoi utiliser un thème enfant ?

Il existe quelques raisons qui pourraient vous donner envie d'utiliser un thème enfant :

- Si vous modifiez un thème existant et qu'il est mis à jour, vos modifications seront perdues.
- Utiliser un thème enfant vous assure que vos modifications seront préservées.
- Utiliser un thème enfant accélère le temps de développement.
- Utiliser un thème enfant est une excellente façon de commencer pour apprendre comment développer un thème WordPress.

#### Comment créer un thème enfant ?

Un thème enfant est composé d'au moins un répertoire et trois fichiers obligatoires. Voici la structure:
- Le répertoire du thème enfant
- style.css
- functions.php
- screenshop.php

1. La première étape dans la création d'un thème enfant est de créer le répertoire du thème enfant, qui sera placé 
dans `wp-content/themes`. Il est recommandé mais pas nécessaire, surtout si vous créez un thème pour un usage public 
d'accoler "-child" à la fin du nom de votre répertoire du thème enfant. 

2. L'étape suivante consiste à créer la feuille de style de votre thème enfant `style.css`. 
La feuille de style doit commencer par les lignes suivantes :

 ```css
 /*
  Theme Name:   Twenty Fifteen Child
  Theme URI:    http://example.com/twenty-fifteen-child/
  Description:  Twenty Fifteen Child Theme
  Author:       John Doe
  Author URI:   http://example.com
  Template:     twentyfifteen
  Version:      1.0.0
  License:      GNU General Public License v2 or later
  License URI:  http://www.gnu.org/licenses/gpl-2.0.html
  Tags:         light, dark, two-columns, right-sidebar, responsive-layout, accessibility-ready
  Text Domain:  twenty-fifteen-child
 */
 ```
 
La ligne `Template` correspond au nom du répertoire du thème parent. Le thème parent dans notre exemple est le thème 
Twenty Fifteen, de sorte que le Template soit twentyfifteen. Vous pouvez travailler avec un thème différent, donc 
adapter en conséquence.

3. La dernière étape consiste à mettre en file d'attente les feuilles de style parents et enfant. La bonne méthode 
de mise en file d'attente de la feuille de style du thème parent est d'utiliser la fonction WordPress 
`wp_enqueue_script()` dans le `functions.php` de votre thème enfant. Vous aurez donc besoin de créer un fichier 
`functions.php` dans le répertoire de votre thème enfant :

```php
<?php
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
    function theme_enqueue_styles() {
        wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    
    }
```

Votre thème enfant est maintenant prêt pour l'activation. Connectez-vous au tableau de bord de votre site, et allez 
dans `Administration Panels > Apparence > Thèmes`. Vous devriez y voir votre thème enfant dans la liste prêt pour l'activation. 