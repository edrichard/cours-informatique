# FOSUserBundle - Ligne de commande

#### Créer un utilisateur 

```
$ php bin/console fos:user:create testuser test@example.com p@ssword
``` 

```
$ php bin/console fos:user:create testuser
```

```
$ php bin/console fos:user:create adminuser --super-admin
``` 

```
$ php bin/console fos:user:create testuser --inactive
```

#### Activer un utilisateur 

```
$ php bin/console fos:user:activate testuser
```

#### Désactiver un utilisateur 

```
php bin/console fos:user:deactivate testuser
```

#### Activer rôle utilisateur 

```
$ php bin/console fos:user:promote testuser ROLE_ADMIN
```

```
$ php bin/console fos:user:promote testuser --super
```

#### Supprimer rôle utilisateur 

```
$ php bin/console fos:user:demote testuser ROLE_ADMIN
```

```
$ php bin/console fos:user:demote testuser --super
```

#### Mettre à jour le mot passe utilisateur

```
$ php bin/console fos:user:change-password testuser newp@ssword
```