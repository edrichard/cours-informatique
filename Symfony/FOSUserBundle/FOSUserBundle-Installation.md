# FOSUserBundle - Installation

#### Pré-requis

Dans le fichier `config.yml`, décommenter la ligne suivante :

```yaml
# app/config/config.yml

framework:
    translator: ~
```

ou

```yaml
# app/config/config.yml

framework:
    translator: { fallbacks: ['%locale%'] }
```

#### Installation

Dan la console, pour télécharger les sources :

```
$ composer require friendsofsymfony/user-bundle "~2.0"
```

Dans le fichier ```app/AppKernel.php``` activer le plugin ```FOSUserBundle``` :

```php
<?php
// app/AppKernel.php

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...

            new FOS\UserBundle\FOSUserBundle(),
        );

        // ...
    }

    // ...
}
```

#### Configuration : `config.yml`

Dans le fichier ```app/config/config.yml``` ajouté :

```yaml
fos_user:
    db_driver: orm # other valid values are 'mongodb' and 'couchdb'
    firewall_name: main
    user_class: AppBundle\Entity\User
    from_email:
        address: 'admin@domain.com'
        sender_name: 'domain.com'
```

#### La classe `User.php`

```php
<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
}
```

#### Configuration `security.yml`

Dans le fichier ```app/config/security.yml``` ajouté ou adapté :

```yaml 
# app/config/security.yml
security:
    encoders:
        FOS\UserBundle\Model\UserInterface: bcrypt

    role_hierarchy:
        ROLE_ADMIN:       ROLE_USER
        ROLE_SUPER_ADMIN: ROLE_ADMIN

    providers:
        fos_userbundle:
            id: fos_user.user_provider.username

    firewalls:
        main:
            pattern: ^/
            form_login:
                provider: fos_userbundle
                csrf_provider: security.csrf.token_manager # Use form.csrf_provider instead for Symfony <2.4
            logout:       true
            anonymous:    true

    access_control:
        - { path: ^/login$, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/register, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/resetting, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/admin/, role: ROLE_ADMIN }
```

#### Routing : `routing.yml`

Dans le fichier ```app/config/routig.yml``` ajouté :

```yaml 
# app/config/routing.yml
fos_user:
    resource: "@FOSUserBundle/Resources/config/routing/all.xml"
```
ou 

```yaml 
# app/config/routing.yml
fos_user_security:
    resource: "@FOSUserBundle/Resources/config/routing/security.xml"

fos_user_profile:
    resource: "@FOSUserBundle/Resources/config/routing/profile.xml"
    prefix: /profile

fos_user_register:
    resource: "@FOSUserBundle/Resources/config/routing/registration.xml"
    prefix: /register

fos_user_resetting:
    resource: "@FOSUserBundle/Resources/config/routing/resetting.xml"
    prefix: /resetting

fos_user_change_password:
    resource: "@FOSUserBundle/Resources/config/routing/change_password.xml"
    prefix: /profile

```

#### Mise à jour de la base de données 

Symfony 2

```
$ php app/console doctrine:schema:update --dump-sql

$ php app/console doctrine:schema:update --force
```

Symfony 3

```
$ php bin/console doctrine:schema:update --dump-sql

$ php bin/console doctrine:schema:update --force
```