# Composer

#### Téléchargement et installation 

```
$ php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
$ php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
$ php composer-setup.php
$ php -r "unlink('composer-setup.php');"
```

#### Erreurs

```
$ composer update

Update failed (The zip extension and unzip command are both missing, skipping.
The php.ini used by your command-line PHP is: /etc/php/7.0/cli/php.ini)

Solution :
$ sudo apt-get install zip unzip php7.0-zip
```


```
$ composer update

Loading composer repositories with package information
Updating dependencies (including require-dev)
  - Installing knplabs/gaufrette (v0.4.0)
The following exception is caused by a lack of memory or swap, or not having swap configured
Check https://getcomposer.org/doc/articles/troubleshooting.md#proc-open-fork-failed-errors for details

                                                     
  [ErrorException]                                   
  proc_open(): fork failed - Cannot allocate memory  
                                                     

update [--prefer-source] [--prefer-dist] [--dry-run] [--dev] [--no-dev] [--lock] [--no-custom-installers] [--no-autoloader] [--no-scripts] [--no-progress] [--no-suggest] [--with-dependencies] [-v|vv|vvv|--verbose] [-o|--optimize-autoloader] [-a|--classmap-authoritative] [--ignore-platform-reqs] [--prefer-stable] [--prefer-lowest] [-i|--interactive] [--root-reqs] [--] [<packages>]...

Solution :
$ free -m
              total        used        free      shared  buff/cache   available
Mem:           2001         121        1219          53         660        1580
Swap:             0           0           0
$ df -h
Filesystem      Size  Used Avail Use% Mounted on
udev            997M     0  997M   0% /dev
tmpfs           201M   34M  167M  17% /run
/dev/vda         46G  2.9G   41G   7% /
tmpfs          1001M     0 1001M   0% /dev/shm
tmpfs           5.0M     0  5.0M   0% /run/lock
tmpfs          1001M     0 1001M   0% /sys/fs/cgroup
tmpfs           201M     0  201M   0% /run/user/1000
$ sudo dd if=/dev/zero of=/swapfile bs=1G count=4
[sudo] password for eddy: 
4+0 records in
4+0 records out
4294967296 bytes (4.3 GB, 4.0 GiB) copied, 25.4165 s, 169 MB/s
$ ls -lh /swapfile
-rw-r--r-- 1 root root 4.0G Jul 20 09:19 /swapfile
$ sudo fallocate -l 4G /swapfile
[sudo] password for eddy: 
eddy@scw-97e5f7:~/produitsensarthe$ sudo chmod 600 /swapfile
eddy@scw-97e5f7:~/produitsensarthe$ ls -lh /swapfile
-rw------- 1 root root 4.0G Jul 20 09:19 /swapfile
$ sudo mkswap /swapfile
Setting up swapspace version 1, size = 4 GiB (4294963200 bytes)
no label, UUID=c7a6cf04-c368-4ec5-996a-1ec9e1843bc0
$ sudo swapon /swapfile
$ sudo swapon -s
Filename				Type		Size	Used	Priority
/swapfile                              	file    	4194300	0	-1
$ free -m
              total        used        free      shared  buff/cache   available
Mem:           2001         124        1237          53         640        1626
Swap:          4095           0        4095
```

