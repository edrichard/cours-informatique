# Le routing


Des URL magnifiques sont absolument indispensables pour toute application Web sérieuse. Cela signifie laisser derrière 
vous des URL saines comme `index.php?article_id=57` en faveur de quelque chose comme `/read/intro-to-symfony`.

La flexibilité est encore plus importante. Que faire si vous devez changer l'URL d'une page de `/blog` to `/news` ? Combien 
de liens devez-vous rechercher et de mettre à jour pour effectuer le changement?  Si vous utilisez le routeur de Symfony, 
le changement est simple.

#### Exemple de routing


Un itinéraire est une carte à partir d'un chemin d'accès URL à un contrôleur. Par exemple, supposons que vous souhaitez associer 
n'importe quelle URL comme `/blog/my-post` ou `/blog/all-about-symfony` et l'envoyer à un contrôleur qui peut rechercher et 
rendre cette entrée de blog. L'itinéraire est simple:

- Avec les Annotations

```php
// src/AppBundle/Controller/BlogController.php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class BlogController extends Controller
{
    /**
     * Matches /blog exactly
     *
     * @Route("/blog", name="blog_list")
     */
    public function listAction()
    {
        // ...
    }

    /**
     * Matches /blog/*
     *
     * @Route("/blog/{slug}", name="blog_show")
     */
    public function showAction($slug)
    {
        // $slug will equal the dynamic part of the URL
        // e.g. at /blog/yay-routing, then $slug='yay-routing'

        // ...
    }
}
```

- Avec YAML

```yaml
# app/config/routing.yml
blog_list:
    path:     /blog
    defaults: { _controller: AppBundle:Blog:list }

blog_show:
    path:     /blog/{slug}
    defaults: { _controller: AppBundle:Blog:show }
```

- Avec XML 

```xml
<!-- app/config/routing.xml -->
<?xml version="1.0" encoding="UTF-8" ?>
<routes xmlns="http://symfony.com/schema/routing"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://symfony.com/schema/routing
        http://symfony.com/schema/routing/routing-1.0.xsd">

    <route id="blog_list" path="/blog">
        <default key="_controller">AppBundle:Blog:list</default>
    </route>

    <route id="blog_show" path="/blog/{slug}">
        <default key="_controller">AppBundle:Blog:show</default>
    </route>
</routes>
```

- Avec PHP : 

```php
// app/config/routing.php
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();
$collection->add('blog_list', new Route('/blog', array(
    '_controller' => 'AppBundle:Blog:list',
)));
$collection->add('blog_show', new Route('/blog/{slug}', array(
    '_controller' => 'AppBundle:Blog:show',
)));

return $collection;
```

Si l'utilisateur se dirige vers `/blog`, la première route est associée et `listAction()` est exécuté.

Si l'utilisateur se dirige vers `/blog/*`, la deuxième route est en correspondance et `showAction()` est exécuté. Parce que le 
chemin d'accès est `/blog/{slug}`, une variable `$slug` est passée à `showAction()` correspondant à cette valeur. Par exemple, 
si l'utilisateur passe à `/blog/yay-routing`, alors `$slug` équivaut à `yay-routing`.

Chaque fois que vous avez un `{placeholder}` dans votre chemin d'itinéraire, cette partie devient un caractère générique : il correspond 
à n'importe quelle valeur. Votre contrôleur peut maintenant avoir un argument appelé `$placeholder` (la base générique et les noms 
d'arguments doivent correspondre).

Chaque route possède également un nom interne: `blog_list` et `blog_show`. Ceux-ci peuvent être n'importe quoi (tant que chacun est 
unique) et n'ont pas encore de sens.

