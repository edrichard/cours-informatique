# Symfony

#### Installation

1. Installation de PHP 7.0

    ```
    $ sudo apt-get php-7.0 libapache2-mod-php7.0
    ```
    
2. Installation de [Composer](composer.md) 
3. Installation de l'instalateur Symfony 

    ```
     $ sudo mkdir -p /usr/local/bin
     $ sudo curl -LsS https://symfony.com/installer -o /usr/local/bin/symfony
     $ sudo chmod a+x /usr/local/bin/symfony
    ```
    
4. Checker PHP pour acceuillir un projet Symfony 

    ```
    $ php bin/symfony_requirements
    ```
    
5. Erreurs 

   ```
   * simplexml_import_dom() must be available
      > Install and enable the SimpleXML extension.

   Solution :
   $ sudo apt-get install php7.0-xml
   ```
   
6. Warning   


   ```
   * PHP-DOM and PHP-XML modules should be installed
      > Install and enable the PHP-DOM and the PHP-XML modules.

   Solution :
   $ sudo apt-get install php7.0-xml
   ```
   
   ```
   * utf8_decode() should be available
      > Install and enable the XML extension.
      
   Solution :
   $ sudo apt-get install php7.0-xml
   ```
   
   ```
   * intl extension should be available
      > Install and enable the intl extension (used for validators).
      
   Solution :
   $ sudo apt-get install php7.0-intl
   ```
   
   ```
   * intl ICU version installed on your system is outdated (57.1) and
      does not match the ICU data bundled with Symfony (59.1)
      > To get the latest internationalization data upgrade the ICU
      > system package and the intl PHP extension.
      
   Solution :
   $ sudo apt-get install build-essential
   $ curl -sS -o /tmp/icu.tar.gz -L http://download.icu-project.org/files/icu4c/[version-icu]/icu4c-[version-icu]-src.tgz 
   $ cd /tmp/
   ...
   ```
   
   ```
   * PDO should have some drivers installed (currently available: none)
     > Install PDO drivers (mandatory for Doctrine).
    
   Solution :
   $ sudo apt-get install php7.0-mysql
   ```

#### Base de données

Reset du mot de passe pour l'utilisateur : `root`

```
$ systemctl set-environment MYSQLD_OPTS="--skip-grant-tables"
$ systemctl restart mariadb
$ mysql_secure_installation
$ mysql 
```
```sql
UPDATE mysql.user SET password=PASSWORD('mon_nouveau_mot_de_passe') WHERE User='root' AND Host = 'localhost';
FLUSH PRIVILEGES;
```
```
$ systemctl unset-environment MYSQLD_OPTS
$ systemctl restart mariadb
```

Connection avec l'utilisateur : `root`

```
$ sudo mysql -u root -p
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 4
Server version: 10.1.23-MariaDB-9+deb9u1 Debian 9.0

Copyright (c) 2000, 2017, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> 
```

Création d'un nouvel utilisateur : 

```
$ sudo mysql -u root -p
Enter password: 

Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 4
Server version: 10.1.23-MariaDB-9+deb9u1 Debian 9.0

Copyright (c) 2000, 2017, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> 
```
```sql
MariaDB [(none)]> CREATE DATABASE [IF NOT EXISTS] {db_name};
Query OK, 1 row affected (0.00 sec)

MariaDB [(none)]> GRANT ALL ON {db_name}.* TO {db_name_user}@localhost IDENTIFIED BY '{p@ssword}';
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]> exit
Bye
```

```
$ mysql -u {db_name_user} -p {db_name}
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 5
Server version: 10.1.23-MariaDB-9+deb9u1 Debian 9.0

Copyright (c) 2000, 2017, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [{db_name}]> exit
Bye
```

#### Configuration serveur

- Serveur : Apache 

Virtual Host Apache :


1. Créer le fichier Virtual Host por Apache :

    ```
    $ sudo nano 001-[non-du-site].conf
    ```

2. Copier et coller le contenu ci-dessou pour la configuration du Virtual Host Apache :

    ```
    <VirtualHost *:80>
    	ServerAdmin support@[non-du-site]
    	ServerName [non-du-site]
    	ServerAlias www.[non-du-site]
    	
    	DocumentRoot /home/[user]/[non-du-site]/web/
    	<Directory /home/[user]/[non-du-site]/web>
    		Require all granted
    		AllowOverride All
            	Allow from All
    
            	<IfModule mod_rewrite.c>
                		Options -MultiViews
                		RewriteEngine On
                		RewriteCond %{REQUEST_FILENAME} !-f
                		RewriteRule ^(.*)$ app.php [QSA,L]
            	</IfModule>
    	</Directory>
    
    	ErrorLog ${APACHE_LOG_DIR}/error-[non-du-site].log
    	CustomLog ${APACHE_LOG_DIR}/access-[non-du-site].log combined
    
    </VirtualHost>
    ```

3. Enregistrer le fichier.

4. Activer le site avec la commande suivante :

    ```
    $ sudo a2ensite 001-[non-du-site].conf
    ```
    
5. Redémarrer le serveur Apache avec la commande : 

    ```
    $ sudo /etc/init.d/apache2 reload
    [ ok ] Reloading apache2 configuration (via systemctl): apache2.service
    ```
6. Mettre les droits en écriture :

    ```
    $ sudo chown www-data:www-data var/sessions/
    $ sudo chown www-data:www-data var/logs/
    $ sudo chown www-data:www-data var/cache/
    
    $ ls -al
    
    drwxr-xr-x 4 www-data www-data  4096 Jul 19 18:03 cache
    drwxr-xr-x 2 www-data www-data  4096 Jul 19 18:04 logs
    drwxr-xr-x 3 www-data www-data  4096 Jul 19 18:04 sessions
    ```

- Serveur : ngnix


#### ACL

1. Installation du package ACL : `$ sudo apt-get install acl`
2. 

    ```
    $ HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1)
    $ sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var
    $ sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var
    ```

#### Commandes utiles

- Vider le cache sur l'environnement de production : `$ php bin/console cache:clear --no-warmup --env=prod`

