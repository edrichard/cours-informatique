# PhoneNumberBundle

Intègre la bibliothèque [libphonenumber de Google](https://github.com/googlei18n/libphonenumber) pour valider les numéros de
téléphone via la bibliothèque PHP [giggsey/libphonenumber-for-php](https://github.com/giggsey/libphonenumber-for-php) dans les 
application Symfony 2 et 3.

#### Installation

Dan la console, pour télécharger les sources :

```
$ composer require misd/phone-number-bundle
```

Dans le fichier ```app/AppKernel.php``` activer le plugin ```PhoneNumberBundle``` :

```php
<?php
// app/AppKernel.php

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...

            new Misd\PhoneNumberBundle\MisdPhoneNumberBundle(),
        );

        // ...
    }

    // ...
}
```

#### Mapping Doctrine

Dans le fichier ```app/config.yml``` ajouté :

```yaml
doctrine:
    dbal:
        types:
            phone_number: Misd\PhoneNumberBundle\Doctrine\DBAL\Types\PhoneNumberType
```

Dans une entitée, pour valider un numéro de téléphone ajouté :

```php
<?php
    /**
     * @ORM\Column(type="phone_number")
     */
    private $phoneNumber;
```

L'entité créera une colonne ```VARCHAR(35)``` en base de données.

#### Format TWIG

```twig
{{ myPhoneNumber|phone_number_format('NATIONAL') }}
```

#### Format PHP

```php 
<?php echo $view['phone_number_format']->format($myPhoneNumber, 'NATIONAL') ?>
```

ou 

```php 
<?php echo $view['phone_number_format']->format($myPhoneNumber, \libphonenumber\PhoneNumberFormat::NATIONAL) ?>
```

#### Validation d'un numéro de téléphone

Valider un numéro de téléphone via une contrainte :

```php 
<?php 
    use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;
    
    /**
     * @AssertPhoneNumber
     */
    private $phoneNumber;
```
ou 

```php 
<?php 
    use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;
    
    /**
     * @AssertPhoneNumber(defaultRegion="FR")
     */
    private $phoneNumber;
```