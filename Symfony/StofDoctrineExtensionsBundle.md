# StofDoctrineExtensionsBundle

Les fonctionnalités de DoctrineExtensions
- **Tree** - cette extension automatise le processus de gestion des arborescences et ajoute certaines fonctions spécifiques 
à l'arborescence sur le référentiel.
- **Translatable** - vous offre une solution très pratique pour traduire les enregistrements en différentes langues. Facile 
à installer, plus facile à utiliser.
- **Sluggable** - urlizes vos champs spécifiés dans un seul slug unique.
- **Timestampable** - met à jour les champs de date sur créer, mettre à jour et même modifier les propriétés.
- **Blameable** - met à jour les chaînes ou les champs d'association sur créer, mettre à jour et même modifier la propriété 
avec un nom d'utilisateur resp. référence.
- **Loggable** - aide à suivre les modifications et l'historique des objets, prend également en charge la gestion de la version.
- **Sortable** - rend un document ou une entité triable
- **Translator** - façon explicite de traiter les traductions
- **Softdeleteable** - permet d'éliminer implicitement les enregistrements
- **Uploadable** - fournit la gestion du chargement des fichiers dans les champs de l'entité
- **Reference Integrity** - fournit une intégrité de référence pour MongoDB, prend en charge ```nullify``` et la ```restrict```.

#### Installation

Dan la console, pour télécharger les sources :

```
$ composer require stof/doctrine-extensions-bundle
```

Dans le fichier ```app/AppKernel.php``` activer le plugin ```StofDoctrineExtensionsBundle``` :

```php
<?php
// app/AppKernel.php

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...

            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
        );

        // ...
    }

    // ...
}
```

#### Configuration du plugin

Dans le fichier ```app/config.yml``` :


```yaml
# stof_doctrine_extensions_bundle
stof_doctrine_extensions:
    orm:
        default:
            sluggable: true
            timestampable: true
```

#### Utilisation de l'extension  ```sluggable``` 

Exemple d'utilisation de l'extension : 

- Ajouter l'annotation ```use Gedmo\Mapping\Annotation as Gedmo;```.
- Sur la propriétée ```$slug``` ajouter l'annotation : ```@Gedmo\Slug(fields={"name"})```, dans ce cas, il prendra la colomme 
```$name``` pour le slugabiliser s'il rencontre des espace il les remplacera par des ```-``` par défaut.

```php
<?php

namespace MC\ProduitsEnSartheBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Slider
 *
 * @ORM\Table(name="slider")
 * @ORM\Entity(repositoryClass="MC\ProduitsEnSartheBundle\Repository\SliderRepository")
 */
class Slider
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", unique=true)
     */
    private $slug;

    // ...

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
     /**
     * Set name
     *
     * @param string $name
     *
     * @return Slider
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Slider
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    // ...
}

```

#### Utilisation de l'option ```timestampable```

Exemple d'utilisation de l'extension : 

- Ajouter l'annotation ```use Gedmo\Mapping\Annotation as Gedmo;```.
- Sur la propriétée de création ```$created```, ajouter l'annotation : ``@Gedmo\Timestampable(on="create")```quand le 
système créera un nouvel objet il mettra la date de création de l'entité.
- Sur la propriétée de mise à jour ```$updated```, ajouter l'annotation : ``@Gedmo\Timestampable(on="update")```quand le 
système mettra à jour l'objet il raffraichira la date de de mise à jour de l'entité.


```php
<?php

namespace MC\ProduitsEnSartheBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Slider
 *
 * @ORM\Table(name="slider")
 * @ORM\Entity(repositoryClass="MC\ProduitsEnSartheBundle\Repository\SliderRepository")
 */
class Slider
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;
    
    // ...

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return Slider
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    // ...

    /**
     * Set created
     * @param \DateTime $created
     * @return Slider
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     * @param \DateTime $updated
     * @return Slider
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
```