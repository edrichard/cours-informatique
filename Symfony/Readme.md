# SYMFONY

[Installation](symfonyInstall.md)

[Composer](composer.md)

[Routing](routing.md)

### Bundles

[StofDoctrineExtensionsBundle](StofDoctrineExtensionsBundle.md)

[PhoneNumberBundle](PhoneNumberBundle.md)

[FOSUserBundle](FOSUserBundle.md)

[FOSUserBundle - Installation](FOSUserBundle/FOSUserBundle-Installation.md)

[FOSUserBundle - Ligne de commandes](FOSUserBundle/FOSUserBundle-CommandTools.md)