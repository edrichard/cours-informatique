# Installer SSL 

- Connexion au back-office de Prestashop

- Cliquer sur l'onglet `Paramètres de la boutique` puis sur `Général`

- Vérifier que la boutique PrestaShop supporte le SSL
   
   Cliquer sur le lien : `Veuillez cliquer ici pour vérifier que votre boutique supporte le protocole HTTPS`, si le 
   protocole SSL est disponible sur votre serveur et si votre certificat SSL est installé, les fonctionnalités 
   ci-dessous s'activeront. 

   ![alt text](img/activer-ssl-prestashop.jpg "activer-ssl-prestashop")

- Activer le SSL sur toute ou partie du PrestaShop
   
   Pour activer le SSL sur le tunnel de conversion (panier, inscription, paiement ...) ainsi que sur le 
   back-office, `placez Activer le SSL` sur `OUI`. Cliquez sur le bouton `Enregistrer`.
   
   ![alt text](img/02-_20170107-165819_1.jpg "02-_20170107-165819_1")
   
   L'activation du SSL déverrouillera `Activer le SSL sur tout le site`. Sélectionnez `OUI` pour sécuriser 
   l'ensemble des pages de votre PrestaShop. Cliquez sur le bouton `Enregistrer`.

   ![alt text](img/activer-ssl-tout-le-site.jpg "activer-ssl-tout-le-site")
   
- Vérifiez que votre template et modules sont compatibles avec le SSL

   Il arrive parfois que ceux-ci ne soient pas parfaitement compatibles avec le SSL. Dans ce cas, le cadenas de 
   sécurité n'apparait pas ou il affiche un avertissement. Cela signifie que votre Prestashop charge des 
   éléments (images, fichiers .css ou .js, polices d'écritures...) en `http` alors que ceux-ci devraient 
   désormais être chargés en `https`.